pub struct Allergies(u32);

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Allergen {
    Eggs = 1,
    Peanuts = 2,
    Shellfish = 4,
    Strawberries = 8,
    Tomatoes = 16,
    Chocolate = 32,
    Pollen = 64,
    Cats = 128,
}

use Allergen::*;
const ALLERGEN: [Allergen; 8] = [
    Eggs,
    Peanuts,
    Shellfish,
    Strawberries,
    Tomatoes,
    Chocolate,
    Pollen,
    Cats,
];

impl Allergies {
    pub fn new(score: u32) -> Allergies {
        Allergies(score)
    }
    pub fn is_allergic_to(&self, allergen: &Allergen) -> bool {
        (self.0 & *allergen as u32) != 0
    }
    pub fn allergies(&self) -> Vec<Allergen> {
        ALLERGEN
            .iter()
            .filter(|x| self.is_allergic_to(x))
            .cloned()
            .collect()
    }
}
